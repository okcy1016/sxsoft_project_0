function _(el) {
    return document.getElementById(el);
}

function uploadFile() {
    var file = _("file_0").files[0];
    // alert(file.name+" | "+file.size+" | "+file.type);
    var formdata = new FormData();
    formdata.append("file_0", file);
    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", "http://127.0.0.1:1508/upload_file");

    // post to gin server
    ajax.send(formdata);
}

function progressHandler(event) {
    _("loaded_n_total").innerHTML = "已上传 " + event.loaded + " bytes of " + event.total;
    var percent = (event.loaded / event.total) * 100;
    _("progressBar").value = Math.round(percent);
    _("status").innerHTML = Math.round(percent) + "% 已上传... 请等待";
}

function completeHandler(event) {
    _("status").innerHTML = event.target.responseText;
    _("progressBar").value = 0; //wil clear progress bar after successful upload
    location.reload();
}

function errorHandler(event) {
    _("status").innerHTML = "上传失败";
}

function abortHandler(event) {
    _("status").innerHTML = "上传终止";
}

