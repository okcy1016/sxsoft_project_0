#!/usr/bin/env python
# coding: utf-8

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["tzd"]
mycol = mydb["obj"]

x = mycol.delete_many({})

print(x.deleted_count, " mongodb rows cleared.")
