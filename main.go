package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"log"
	"fmt"
	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-sqlite3"
	"strings"
	"time"
	"html/template"
	"strconv"
	"os/exec"
	"os"
	"math/rand"
	"flag"
)

// Describe a struct to store task status
type SpiderTask struct {
	ID int
	TaskName string
	InitFileName string
	ResultFileName string
	ProcessStatus string
	TransferStatus string
	CreateTime string
	Password string
}

// Declare these variables globally
var orm *xorm.Engine
var scpRemoteDirPtr *string

// Declare a variale to indicate which task is and should be running
var runTaskID int

func main() {
	// Flag declarations
	scpRemoteDirPtr = flag.String("scp-to-remote", "", "Remote dir which scp upload to.\nExample: --scp-to-remote ic@192.168.200.11:/home/ic/tomcat8-www/webapps/ROOT/static/report")
	// Parse flags
	flag.Parse()
	
	//// Setup database
	var err error	
	// Create tasks_status dir if it doesn't exist
	if _, err = os.Stat("./tasks_status"); err != nil {
		os.Mkdir("./tasks_status", 0755)
	}
	
	orm, err = xorm.NewEngine("sqlite3", "./tasks_status/tasks_sqlite3.db")
	CheckErr(err)
	// orm.ShowSQL(true)
	defer orm.Close()

	// Initiate runTaskID to zero indicates no task should be run
	runTaskID = 0

	// Create task status table if it doesn't exist
	err = orm.CreateTables(&SpiderTask{})
	CheckErr(err)
	
	r := SetupRouter()
	r.Run(":1508")
}

func SetupRouter() *gin.Engine {

	// Set gin in release mode
	gin.SetMode(gin.ReleaseMode)
	// Diable console color
	gin.DisableConsoleColor()
	////  Setup routes
	router := gin.New()
	// Recovery middleware recovers from any panics and writes a 500 if there was none
	// Disabled logger
	router.Use(gin.Recovery())
	
	// Serve static files and templates
	router.LoadHTMLGlob("./html/templates/*")
	router.Static("/css", "./html/css")
	router.Static("/js", "./html/js")
	router.GET("/", func(c *gin.Context) {
		//// Get task history from database first
		// declare a spiderTask slice
		var spiderTaskSlice []SpiderTask
		err := orm.Find(&spiderTaskSlice)
		CheckErr(err)
		
		// Get table html string from database
		tableContents := BuildTableString(spiderTaskSlice)

		c.HTML(http.StatusOK, "index.html", gin.H {
			"tableContents": template.HTML(tableContents),
		})
	})
	
	router.POST("/UploadFile", PostUpload)
	router.GET("/RunTaskAgain/:id_", GetRunTaskAgain)
	router.GET("/DeleteTask/:id_", GetDeleteTask)
	router.GET("/ClearAll", GetClearAll)
	return router
}

func CheckErr(err error) {
	if err != nil {
		log.Println(err)
		panic(err)
	}
}

func BuildTableString(spiderTaskSlice []SpiderTask) string {
	// Declare a string builder
	var strBldr strings.Builder
	
	// Build table string
	for ord_ := range spiderTaskSlice {
		strBldr.WriteString("<tr>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(strconv.Itoa(spiderTaskSlice[ord_].ID))
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].TaskName)
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].InitFileName)
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].ResultFileName)
		strBldr.WriteString("<br>Key: ")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].Password)
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].ProcessStatus)
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].TransferStatus)
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].CreateTime)
		strBldr.WriteString("</td>")
		strBldr.WriteString("<td>")
		strBldr.WriteString("<a id=\"operationLink\" href=\"/RunTaskAgain/" + strconv.Itoa(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].ID) + "\">重新执行</a><br><a id=\"operationLink\" href=\"/DeleteTask/" + strconv.Itoa(spiderTaskSlice[len(spiderTaskSlice)-1-ord_].ID) + "\">删除</a>")
		strBldr.WriteString("</td>")
		strBldr.WriteString("</tr>")
	}

	return strBldr.String()
}

func StartTask(fileName string, id_ int) {
	// Clear mongodb database
	clearDBCmd := exec.Command("python3", "./scripts/clear_mongodb.py")
	clearDBCmdOut, err := clearDBCmd.CombinedOutput()
	fmt.Println(string(clearDBCmdOut))
	CheckErr(err)
	
	// Unzip with password, "-o" for overwrite
	unzipCmd := exec.Command("unzip", "-o", "-P", "lyy#2017", "./upload/" + fileName, "-d", "./upload")
	unzipCmdOut, err := unzipCmd.CombinedOutput()
	fmt.Println(string(unzipCmdOut))
	CheckErr(err)
	
	// Import urls
	importURLCmd := exec.Command("python", os.Getenv("HOME") + "/videoSpider/urls/url_import.py", "./upload/" + fileName[0:len(fileName)-4])
	importURLCmdOut, err := importURLCmd.CombinedOutput()
	fmt.Println(string(importURLCmdOut))
	CheckErr(err)
	
	// Run spider
	log.Println("Running video spider at first time without proxy ...")
	runSpiderCmd := exec.Command("python", os.Getenv("HOME") + "/videoSpider/urls/run_spider.py")
	runSpiderCmdOut, err := runSpiderCmd.CombinedOutput()
	fmt.Println(string(runSpiderCmdOut))
	CheckErr(err)

	//// Generate csv in non-proxy mode
	// Construct long parameter string
	longParam := fmt.Sprintf(`{"batch_id": {"$gte": "%s", "$lte": "%s"}}`, fileName[0:8] + "00", fileName[9:17] + "00")
	
	genCsvNonProxyCmd := exec.Command("mongoexport", "-d", "tzd", "-c", "obj", "-f", "batch_id,webpage_url,fulltitle,ext,relpath,md5,crawled_date,error_report", "-q", longParam, "--csv", "-o", os.Getenv("HOME") + "/videoSpider/urls/Report_" + fileName[0:8] + "_to_" + fileName[9:17] + ".csv")
	genCsvNonProxyCmdOut, err := genCsvNonProxyCmd.CombinedOutput()
	fmt.Println(string(genCsvNonProxyCmdOut))
	CheckErr(err)
	log.Println("Non-proxy csv result file generated.")

	// Export error urls
	// Construct long parameter string
	longParam = fmt.Sprintf(`{"$and":[{"error_report":{"$ne":""}},{"error_report":{"$ne":"Unsupported URL"}}],"batch_id": {"$gte": "%s00", "$lte": "%s00"}}`, fileName[0:8], fileName[9:17])

	// Create error urls folder if it not exists
	errorFolderPath := os.Getenv("HOME") + "/videoSpider/urls/test" + fileName[0:8] + "-" + fileName[9:17]
	if _, err := os.Stat(errorFolderPath); os.IsNotExist(err) {
		os.Mkdir(errorFolderPath, 0755)
	}
	
	genErrorCsvCmd := exec.Command("mongoexport", "-d", "tzd", "-c", "obj", "-f", "batch_id,webpage_url", "-q", longParam, "--csv", "-o", os.Getenv("HOME") + "/videoSpider/urls/test" + fileName[0:8] + "-" + fileName[9:17] + "/test_" + fileName[0:8] + "_to_" + fileName[9:17] + ".csv")
	genErrorCsvCmdOut, err := genErrorCsvCmd.CombinedOutput()
	fmt.Println(string(genErrorCsvCmdOut))
	CheckErr(err)
	log.Println("Error urls csv result file generated.")
	
	// Clear mongodb again
	clearDBCmd = exec.Command("python3", "./scripts/clear_mongodb.py")
	clearDBCmdOut, err = clearDBCmd.CombinedOutput()
	fmt.Println(string(clearDBCmdOut))
	CheckErr(err)

	// Start shadowsocks
	// Terminate on exit
	runSSCmd := exec.Command(os.Getenv("HOME") + "/Desktop/Shadowsocks-Qt5-3.0.1-x86_64.AppImage")
	runSSCmd.Start()

	// Wait 3 second for it to start
	log.Println("Wait 3 second for it to start ...")
	time.Sleep(5 * time.Second)

	// Re-import error urls
	importErrURLCmd := exec.Command("python", os.Getenv("HOME") + "/videoSpider/urls/url_import.py", errorFolderPath)
	importErrURLCmdOut, err := importErrURLCmd.CombinedOutput()
	fmt.Println(string(importErrURLCmdOut))
	CheckErr(err)
	
	// Run spider with proxy
	log.Println("Running spider at second time with proxy for the error urls ...")
	runSpiderWithProxyCmd := exec.Command("python", os.Getenv("HOME") + "/videoSpider/urls/run_spider_with_proxy.py")
	runSpiderWithProxyCmdOut, err := runSpiderWithProxyCmd.CombinedOutput()
	fmt.Println(string(runSpiderWithProxyCmdOut))
	CheckErr(err)

	//// Generate csv in proxy mode
	// Construct long parameter string
	longParam = fmt.Sprintf(`{"batch_id": {"$gte": "%s", "$lte": "%s"}}`, fileName[0:8] + "00", fileName[9:17] + "00")
	
	genCsvProxyCmd := exec.Command("mongoexport", "-d", "tzd", "-c", "obj", "-f", "batch_id,webpage_url,fulltitle,ext,relpath,md5,crawled_date,error_report", "-q", longParam, "--csv", "-o", os.Getenv("HOME") + "/videoSpider/urls/Report_" + fileName[0:8] + "_to_" + fileName[9:17] + "_proxy.csv")
	genCsvProxyCmdOut, err := genCsvProxyCmd.CombinedOutput()
	fmt.Println(string(genCsvProxyCmdOut))
	CheckErr(err)
	log.Println("Proxy mode csv result file generated.")

	// Shutdown shadowsocks-qt5
	stopSSCmd := exec.Command("killall", "ss-qt5")
	stopSSCmdOut, err := stopSSCmd.CombinedOutput()
	fmt.Println(string(stopSSCmdOut))
	CheckErr(err)

	// Make final result dir
	resDir := os.Getenv("HOME") + "/videoSpider/output_result/" + fileName[9:13] + "-" + fileName[13:15] + "-" + fileName[15:17]
	if _, err := os.Stat(resDir); os.IsNotExist(err) {
		os.Mkdir(resDir, 0755)
	}
	if _, err := os.Stat(resDir+"/JingNei"); os.IsNotExist(err) {
		os.Mkdir(resDir+"/JingNei", 0755)
	}
	if _, err := os.Stat(resDir+"/JingWai"); os.IsNotExist(err) {
		os.Mkdir(resDir+"/JingWai", 0755)
	}

	//// Move contents to final result dir
	// Copy non-proxy contents
	// Get paths
	// Get year, month, day firstly
	timeStr := time.Now().Format(time.RFC3339)
	yearStr := timeStr[0:4]
	monthStr := timeStr[5:7]
	dayStr := timeStr[8:10]
	nonProxyContentsPath := os.Getenv("HOME") + "/videoSpider/" + yearStr + "-" + monthStr + "-" + dayStr + "/."

	// Non-proxy csv file
	nonProxyCsvPath := os.Getenv("HOME") + "/videoSpider/urls/Report_" + fileName[0:8] + "_to_" + fileName[9:17] + ".csv"
	
	// Copy proxied contents to result dir
	// Get paths
	proxiedContentsPath := os.Getenv("HOME") + "/videoSpider/proxy/" + yearStr + "-" + monthStr + "-" + dayStr + "/."

	// Proxied csv file
	proxiedCsvPath := os.Getenv("HOME") + "/videoSpider/urls/Report_" + fileName[0:8] + "_to_" + fileName[9:17] + "_proxy.csv"

	// Start copying non-proxy files
	copyNonProxyContentsCmd := exec.Command("bash", "-c", fmt.Sprintf("cp -f -r %s %s/JingNei", nonProxyContentsPath, resDir))
	copyNonProxyContentsCmdOut, err := copyNonProxyContentsCmd.CombinedOutput()
	fmt.Println(string(copyNonProxyContentsCmdOut))
	CheckErr(err)
	
	copyNonProxyCsvCmd := exec.Command("bash", "-c", fmt.Sprintf("cp -f %s %s/JingNei", nonProxyCsvPath, resDir))
	copyNonProxyCsvCmdOut, err := copyNonProxyCsvCmd.CombinedOutput()
	fmt.Println(string(copyNonProxyCsvCmdOut))
	CheckErr(err)

	// Start copying proxied files
	copyProxiedContentsCmd := exec.Command("bash", "-c", fmt.Sprintf("cp -f -r %s %s/JingWai", proxiedContentsPath, resDir))
	copyProxiedContentsCmdOut, err := copyProxiedContentsCmd.CombinedOutput()
	CheckErr(err)
	fmt.Println(string(copyProxiedContentsCmdOut))

	copyProxiedCsvCmd := exec.Command("bash", "-c", fmt.Sprintf("cp -f %s %s/JingWai", proxiedCsvPath, resDir))
	copyProxiedCsvCmdOut, err := copyProxiedCsvCmd.CombinedOutput()
	fmt.Println(string(copyProxiedCsvCmdOut))
	CheckErr(err)
	log.Println("File copied to output_result.")

	// Delete compressed file if exists
	if _, err := os.Stat(resDir+".7z"); !os.IsNotExist(err) {
		os.Remove(resDir+".7z")
	}

	// Select from database to obtain password
	var spiderTask SpiderTask
	_, err = orm.Where("i_d = ?", id_).Get(&spiderTask)
	
	// Start compress files
	_7zCompressCmd := exec.Command("7za", "a", "-t7z", "-r", "-p"+spiderTask.Password, "-mhe=on", resDir+".7z", resDir)
	_7zCompressCmdOut, err := _7zCompressCmd.CombinedOutput()
	fmt.Println(string(_7zCompressCmdOut))
	CheckErr(err)
	log.Println("File compressed.")

	// Store password at local
	// If the file doesn't exist, create it, or append to the file
	fpw, err := os.OpenFile(resDir+"/../password", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	CheckErr(err)
	_, err = fpw.Write([]byte("\n" + fileName[9:13] + "-" + fileName[13:15] + "-" + fileName[15:17] + ".7z" + "   " + spiderTask.Password))
	CheckErr(err)
	fpw.Close()

	// Whether start tranfering through scp
	scpRemoteDir := *scpRemoteDirPtr
	if scpRemoteDir != "" {
		log.Println("Start to transfer through scp ...")
		scpCmd := exec.Command("sshpass", "-p", "cxzx2017", "scp", resDir+".7z", scpRemoteDir)
		scpCmdOut, err := scpCmd.CombinedOutput()
		fmt.Println(string(scpCmdOut))
		CheckErr(err)
		log.Println("scp transfer finished.")
	} else {
		// If not scp then copy the local http serve dir
		log.Println("Start copying result file to local http serve dir ...")
		copyResultFileToHTTPServeDirCmd := exec.Command("bash", "-c", fmt.Sprintf("cp -f -r %s %s", resDir+".7z", os.Getenv("HOME")+"/tomcat8-www/webapps/ROOT/static/report"))
		copyResultFileToHTTPServeDirCmdOut, err := copyResultFileToHTTPServeDirCmd.CombinedOutput()
		CheckErr(err)
		fmt.Println(string(copyResultFileToHTTPServeDirCmdOut))
		log.Println("Copy finished.")
	}

	// Update data
	// Do not change Transfer status when without --scp-to-remote flag
	if scpRemoteDir != "" {
		spiderTask.TransferStatus = "已传送"
	} else {
		spiderTask.TransferStatus = "已复制"
	}
	spiderTask.ProcessStatus = "已完成"
	_, err = orm.Update(&spiderTask, &SpiderTask{ID:id_})
	CheckErr(err)

	// Clear mongodb database
	clearDBCmd = exec.Command("python3", "./scripts/clear_mongodb.py")
	clearDBCmdOut, err = clearDBCmd.CombinedOutput()
	fmt.Println(string(clearDBCmdOut))
	CheckErr(err)

	// Check if there's another task to run
	// If exists, then run it
	// Clear spiderTask struct first
	spiderTask = SpiderTask{}
	ifTaskNotDone, err := orm.Where("process_status = ?", "运行中").Get(&spiderTask)
	CheckErr(err)
	if ifTaskNotDone == true {
		log.Println("Ready to run next task with id " + strconv.Itoa(spiderTask.ID) + ".")
		runTaskID = spiderTask.ID
	} else {
		runTaskID = 0
		log.Println("All tasks are done.")
	}
}

func GeneratePassword() string {
	// Generate random 13 bit 7z password
	// Get rand seed by current time
	rand.Seed(time.Now().UnixNano())
	letters_ := []rune("0123456789abcdefghijklmnopqrstuvwxyz")
	tmpPassword := make([]rune, 13)
	for ord_ := range tmpPassword {
		tmpPassword[ord_] = letters_[rand.Intn(len(letters_))]
	}
	password := string(tmpPassword)
	return password
}

func StartTaskInRoutine(spiderTask SpiderTask) {
	go func() {
		// Blocking while there is another task running
		for {
			if runTaskID == spiderTask.ID {
				break
			} else {
				time.Sleep(time.Second)
			}
		}
		
		// Main task
		StartTask(spiderTask.InitFileName, spiderTask.ID)
	}()
}

func PostUpload(c *gin.Context) {
	// Check whether file is selected
	file, err := c.FormFile("task_init_file")
	if err != nil {
		log.Println("detected upload without file selected.")
		htmlStr := `<html><head><meta http-equiv="refresh" content="2;url=http://127.0.0.1:1508" /></head><body><h3>请先选择文件！</h3></body></html>`
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write([]byte(htmlStr))
		return
	}
	
	log.Println(file.Filename)

	// Create upload folder if it not exists
	if _, err := os.Stat("./upload"); err != nil {
		os.Mkdir("./upload", 0755)
	}
	
	// Upload file to specific dst.
	c.SaveUploadedFile(file, "./upload/" + file.Filename)

	// Check if it already exists
	ifAlreadyExist, err := orm.Exist(&SpiderTask{InitFileName:file.Filename})
	CheckErr(err)
	if ifAlreadyExist == true {
		htmlStr := `<html><head><meta http-equiv="refresh" content="2;url=http://127.0.0.1:1508" /></head><body><h3>该任务已存在！</h3></body></html>`
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write([]byte(htmlStr))
		return
	}
	
	//// Store task status to database
	// Get ID number from database firstly
	var spiderTaskSlice []SpiderTask
	err = orm.Find(&spiderTaskSlice)
	CheckErr(err)

	// Find max id number
	maxIDNum := 0
	for _ord := range spiderTaskSlice {
		if spiderTaskSlice[_ord].ID > maxIDNum {
			maxIDNum = spiderTaskSlice[_ord].ID
		}
	}
	var spiderTask SpiderTask
	spiderTask.ID = maxIDNum + 1

	// cut the filename suffix ".zip"
	spiderTask.TaskName = file.Filename[0:len(file.Filename)-4]
	
	spiderTask.InitFileName = file.Filename

	// Construct result file hyperlink
	resultFileHyperLink := "http://220.171.93.252:8888/static/report/" + file.Filename[9:13] + "-" + file.Filename[13:15] + "-" + file.Filename[15:17] + ".7z"
	// Embed hyper link into result file name
	spiderTask.ResultFileName = fmt.Sprintf(`<a href=%s>%s</a>`,resultFileHyperLink, file.Filename[9:13] + "-" + file.Filename[13:15] + "-" + file.Filename[15:17] + ".7z")
	spiderTask.ProcessStatus = "运行中"
	spiderTask.TransferStatus = "未传送"
	currentTimeStr := time.Now().Format(time.RFC3339)
	spiderTask.CreateTime = currentTimeStr[0:len(currentTimeStr)-6]

	// Generate result file password
	resultFilePassword := GeneratePassword()
	spiderTask.Password = resultFilePassword

	// Inserting
	_, err = orm.Insert(&spiderTask)
	CheckErr(err)

	// Redirect back
	time.Sleep(1 * time.Second)
	c.Redirect(http.StatusSeeOther, "/")
	
	// Start routine
	if runTaskID == 0 {
		runTaskID = spiderTask.ID
	}
	StartTaskInRoutine(spiderTask)
}

func GetRunTaskAgain(c *gin.Context) {
	id_ := c.Param("id_")

	// Get a row accroding to this id_
	var spiderTask SpiderTask
	idNum, err := strconv.Atoi(id_)
	_, err = orm.Where("i_d = ?", idNum).Get(&spiderTask)
	CheckErr(err)

	// Restart only when the task is done.
	if spiderTask.ProcessStatus == "已完成" {
		// Restore processing status
		spiderTask.ProcessStatus = "运行中"
		spiderTask.TransferStatus = "未传送"
		_, err = orm.Update(&spiderTask, &SpiderTask{ID:idNum})
		CheckErr(err)

		// Start routine
		if runTaskID == 0 {
			runTaskID = spiderTask.ID
		}
		StartTaskInRoutine(spiderTask)
		
		time.Sleep(1 * time.Second)
		c.Redirect(http.StatusSeeOther, "/")
	} else {
		htmlStr := `<html><head><meta http-equiv="refresh" content="3;url=http://127.0.0.1:1508" /></head><body><h3>只有执行完成的任务才能重新运行！</h3></body></html>`
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write([]byte(htmlStr))
	}
}

func GetDeleteTask(c *gin.Context) {
	id_ := c.Param("id_")
	idNum, err := strconv.Atoi(id_)
	CheckErr(err)

	// Only can delete when it's done
	var spiderTask SpiderTask
	_, err = orm.Where("i_d = ?", idNum).Get(&spiderTask)
	CheckErr(err)
	if spiderTask.ProcessStatus == "已完成" {
		// Delete row specific row from database
		_, err = orm.Where("i_d = ?", idNum).Delete(&SpiderTask{})
		CheckErr(err)
		
		time.Sleep(1 * time.Second)
		c.Redirect(http.StatusSeeOther, "/")
	} else {
		htmlStr := `<html><head><meta http-equiv="refresh" content="3;url=http://127.0.0.1:1508" /></head><body><h3>只有执行完成的任务才能删除！</h3></body></html>`
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write([]byte(htmlStr))
	}
}

func GetClearAll(c *gin.Context) {
	if runTaskID != 0 {
		htmlStr := `<html><head><meta http-equiv="refresh" content="3;url=http://127.0.0.1:1508" /></head><body><h3>清空失败！当前还有未完成的任务。<br>(不包括中途被强行停止的任务)</h3></body></html>`
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write([]byte(htmlStr))
		return
	}
	// Clear database table spider_task
	_, err := orm.Exec("delete from spider_task")
	CheckErr(err)

	// Remove contents in folder ./upload
	err = os.RemoveAll("./upload/")
	CheckErr(err)

	time.Sleep(time.Second)
	c.Redirect(http.StatusSeeOther, "/")
}
