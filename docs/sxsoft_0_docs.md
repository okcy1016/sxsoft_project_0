# 2018-10-29 02:11 修复与更新
1. 优化了任务 ID 的创建方式。
2. 将任务列表按照时间倒序排列。

# 2018-10-26 21:53 更新
1. 结果文件的密码实时显示在网页上。
2. 添加自定义选项 `--scp-to-remote` 来决定是否在完成后进行 scp 远程上传。格式为 `--scp-to-remote username@xxx.xxx.xxx.xxx:/dir`。
3. 移除了点击文件下载链接所弹出的操作成功完成提示。

# 2018-09-29 16:37 更新与修复
1. 修正为在网页上显示结果文件下载链接。
2. 添加了删除所有任务与上传文件的按钮。（方便应用于这种情况：后端程序在任务运行中途意外中止，重启之前的任务一直显示 “运行中” 且无法删除。）
3. 检测处理了未选择文件却上传的行为。
4. 增加了结果文件下载的超链接。

# 2018-09-29 05:43 问题反馈
1. 由于 `python run_spider.py` 或 `python run_spider_proxy.py` 所爬取的文件保存在以当前日期命名的文件夹中，这样将会导致：若在一天中同时上传创建了两个不同日期的的初始文件的任务，那么这两个任务最终的产生文件将是一样的！且是这两个不同初始文件所爬取的总内容。

# 2018-09-29 05:33 更新修复
1. 调整了代码使得程序执行外部命令时，若出现异常并退出，能够输出相关外部命令的出错输出，能够更好、更快地找到问题原因。

# 2018-09-28 14:00 更新修复
1. 修复了 cp 命令在复制空文件下的所有内容时出现找不到文件的错误，將命令由 `cp /xxxx/empty_dir/*  /destination_dir` 修改为 `cp /xxxx/empty_dir/.  /xxxx/destination_dir/` 。
2. 为最终结果 7z 文件生成 13 位随机密码，密码文件保存于 `/home/ic/videoSpider/output_result/password` 。 

# 使用方法
注意：以下操作都是在爬虫运行的服务器上进行的。

1. 进入目录执行 web 管理界面后台程序，爬虫程序的输出将会显示在当前控制台中。
```shell
cd /home/ic/videoSpider/web_ctrl_crawler/
./web_ctrl_crawler
```
2. 打开浏览器，访问 http://127.0.0.1:1508/ 即可进入 web 管理界面。

# 任务创建后程序自动流程详情
1. 初始化 web 后端。
2. 新任务上传后：
* 2.1 清空 mongodb 数据库 obj 中所有内容。
* 2.2 `unzip` 解压带有密码的初始 url 压缩文件。
* 2.3 运行 `python $HOME/videoSpider/urls/url_import.py` 以导入url。
* 2.4 开始运行第一次爬取（不通过代理）， `python $HOME/videoSpider/urls/run_spider.py` 。
* 2.5 生成 csv 文件（不经过代理），格式为 Report_201xxxxx_to_201xxxxx.csv（以初始文件名中的第二个日期命名），位置为 `$HOME/videoSpider/urls/` 。
* 2.6 导出出错的 url 列表到目录 ，`$HOME/videoSpider/urls/test201xxxxx/` （以爬取日期命名）。
* 2.7 再次清空 mondb 数据库 obj 中所有内容。
* 2.8 开启代理，执行 `$HOME/Desktop/Shadowsocks-Qt5-3.0.1-x86_64.AppImage` 。
* 2.9 导入出错的 url 文件，`python $HOME/videoSpider/urls/url_import.py` 。
* 2.10 开始第二次爬取（使用代理），`python $HOME/videoSpider/urls/run_spider_with_proxy.py`。
* 2.11 生成 csv 文件（使用代理），格式为 Report_201xxxxx_to_201xxxxx_proxy.csv（以初始文件名中的第二个日期命名），位置为 `$HOME/videoSpider/urls/` 。
* 2.12 停止代理，执行 `killall ss-qt5` 。
* 2.13 复制本次爬取的文件到汇总目录，`$HOME/videoSpider/output_result/` 。
* 2.14 压缩最终文件，文件生成位置 `$HOME/videoSpider/output_result/` ，文件名 201x-xx-xx.7z（以初始文件名中的第二个日期命名），压缩密码为为随机生成的 13bit 字符串。
* 2.15 若用户使用了 `--scp-to-remote username@xxx.xxx.xxx.xxx:/dir` 则使用 scp 传输最终文件至服务器。
* 2.16 最后清空 mongodb 数据库 obj 中所有内容。
3. 停止或执行下一个任务。

## 说明：
1. 由于只有一个 mongodb 数据库进行数据存取，所以不能实现多爬取任务同时运行，只能于任务列表中依次执行。
2. 还没有实现任务运行中途中止的功能。
3. 还有什么需求可以提。
4. 希望以后还有能够合作的机会。
