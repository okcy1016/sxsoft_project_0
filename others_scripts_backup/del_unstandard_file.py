#!/usr/bin/env python
# coding: utf-8

import datetime
import os


def del_unstandard_file(type_=0):

    """
    删除不在标准音视频文件列表里的文件，以及空的文件夹
    标准音视频文件列表为：['mp4','flv','ogg','webm','mkv','avi','aac','flac','mp3','m4a','opus','wav']

    type_
    0:没有使用代理运行
    1:使用代理运行

    """

    file_num = 0#标准音视频文件的数量
    year = datetime.datetime.now().year
    month = str(datetime.datetime.now().month) if datetime.datetime.now().month >= 10 else "0"+str(datetime.datetime.now().month)
    day = str(datetime.datetime.now().day) if datetime.datetime.now().day >= 10 else "0"+str(datetime.datetime.now().day)
    now_time = str(year) + "-" + month + "-" + day

    now_path = os.getcwd()
    print "now_path",now_path
    if type_ == 0:
        # Modified by neverstop for use in other pwd paths
        # date_file = os.path.dirname(now_path)+'/'
        date_file = "/home/ic/videoSpider/"
    elif type_ == 1:
        # Modified by neverstop for use in other pwd paths
        # date_file = os.path.dirname(now_path) + '/proxy/'
        date_file = "/home/ic/videoSpider/proxy/"
    print "date_file",date_file
    # date_file = '/home/ic/videoSpider/'
    path_ = date_file+now_time
    with open(date_file + 'produced_file.log','a+') as f:
        f.write(now_time+'\n')

    # print "mulu:",path_
    if os.path.exists(path_) is False:
        print
        print path_,'目录不存在!'
        with open(date_file + 'produced_file.log', 'a+') as f:
            f.write(path_+'   is not exist!标准音视频文件数量为:0'+'\n')
        print
        print '标准音视频文件数量为:0'
        print
        print '执行,"删除不在标准音视频文件列表里的文件,以及空的文件夹" 程序完毕!'
        return
    # a = os.path.walk(path)
    FileNames = os.listdir(path_)
    # print "FileNames:",FileNames
    for file_name in FileNames:
        fullfilename = os.path.join(path_, file_name)
        # print "fullfilename:",fullfilename

        if len(os.listdir(fullfilename)) == 0:
            with open(date_file + 'produced_file.log', 'a+') as f:#把产生的空文件夹记录在produced_file.log文件里，方便后续查看、比对
                f.write(fullfilename+'\n')
        for line in os.listdir(fullfilename):
            doc_path = os.path.join(fullfilename, line)
            with open(date_file + 'produced_file.log', 'a+') as f:#把产生的文件记录在produced_file.log文件里，方便后续查看、比对
                f.write(doc_path+'\n')

            # print "doc_path:",doc_path
            doc_name = os.path.basename(doc_path)
            # print "doc_name:",doc_name
            if "." in doc_name:
                doc_type = doc_name.split(".")[-1] #os.path.splitext(path)
                if doc_type in ['mp4','flv','ogg','webm','mkv','avi','aac','flac','mp3','m4a','opus','wav']:
                    print doc_path,'该文件是标准文件'
                    file_num += 1
                else:
                    print doc_path,'该文件不是标准文件'
                    os.remove(doc_path)
                    print '删除不标准文件成功,不标准文件为:',doc_path
                # print "doc_type:",doc_type
            else:
                print doc_path, '该文件不是标准文件'
                # print "doc_:",doc_name
                os.remove(doc_path)
                print '删除不标准文件成功,不标准文件为:',doc_path

        if len(os.listdir(fullfilename)) == 0:#没有标准音视频文件的空文件夹也要删除
            os.removedirs(fullfilename)
            print '删除空文件夹成功,空文件夹为:',fullfilename

    if os.path.exists(path_) is False:
        print
        print '标准音视频文件数量为:0'
        os.mkdir(path_)#用程序删除掉文件夹下所有的文件夹和文件后，该文件夹也会不存在，所以要创建这个文件夹
    else:
        if len(os.listdir(path_)) == 0:
            print
            print '标准音视频文件数量为:0'

    if file_num == 0:
        with open(date_file + 'produced_file.log', 'a+') as f:
            f.write('标准音视频文件数量为:0!' + '\n')
    else:
        with open(date_file + 'produced_file.log', 'a+') as f:
            f.write('标准音视频文件数量为:'+str(file_num)+ '\n')
        print
        print '标准音视频文件数量为:' + str(file_num)

    print
    print '执行,"删除不在标准音视频文件列表里的文件,以及空的文件夹" 程序完毕!'


if __name__ == '__main__':
    del_unstandard_file(1)
