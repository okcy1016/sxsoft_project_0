#!/usr/bin/env python
# coding: utf-8

import pymongo
import subprocess
import del_unstandard_file

client = pymongo.MongoClient('localhost', 27017)
collection = client["tzd"]["obj"]

for item in collection.find():
    if not item.has_key('crawled_date'):
        url = item['webpage_url']
        command = 'youtube-dl "' + url + '" -i --no-part --socket-timeout 3 --config-location /home/ic/videoSpider/urls/youtube-dl.conf --exec "python /home/ic/videoSpider/urls/calc_file_md5.py {}" --write-info-json'
        #print command
        subprocess.call(command, shell = True)

del_unstandard_file.del_unstandard_file(1)
