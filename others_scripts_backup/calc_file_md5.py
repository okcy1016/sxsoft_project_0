import os
import sys
import hashlib
import pymongo
import time

_FILE_SLIM = (100*1024*1024) # 100MB


def file_md5(filename):
    calltimes = 0
    hmd5 = hashlib.md5()
    fp = open(filename,"rb")
    f_size = os.stat(filename).st_size
    if f_size>_FILE_SLIM:
        while(f_size>_FILE_SLIM):
            hmd5.update(fp.read(_FILE_SLIM))
            f_size/=_FILE_SLIM
            calltimes += 1   #delete
        if(f_size>0) and (f_size<=_FILE_SLIM):
            hmd5.update(fp.read())
    else:
        hmd5.update(fp.read())    
    return (hmd5.hexdigest(),calltimes)

def filter_video(filename):
   Extensions = ('mp4','flv','ogg','webm','mkv','avi','aac','flac','mp3','m4a','opus','wav')
   (filepath,tempfilename) = os.path.split(filename)
   (fname,fextension) = os.path.splitext(tempfilename)
   if fextension[1:] not in Extensions:
       os.remove(filename)
       print "after remove:%s"%os.listdir(os.getcwd())
       return False
   else: 
       return True
   
    

if __name__ == '__main__':
    if len(sys.argv) == 2:
        filepath = os.path.abspath(sys.argv[1])
        (hvalue,ctimes) = file_md5(filepath)
        download_date = time.ctime(os.stat(sys.argv[1]).st_ctime)
        download_dat = time.strftime("%Y-%m-%d" , time.strptime(download_date))
        relpath = filepath[20:]
#        print(relpath)
#        print(sys.argv[0])
#        print(sys.argv[1])
#        print(hvalue)
        client =pymongo.MongoClient("127.0.0.1",27017)
        db = client["tzd"]
        users = db["obj"]
        users.update({"_filename":sys.argv[1]},{"$set":{"relpath":relpath,"download_date":download_dat,"md5":hvalue}},False,False) 
#        if not filter_video(filepath):
#            users.remove({"_filename":sys.argv[1]})   
     
