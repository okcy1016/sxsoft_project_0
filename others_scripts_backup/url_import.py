#!/usr/bin/env python
# -*- coding:utf-8 -*-
#
#   Author  :   Yangyang Li
#   E-mail  :   liyangyang@live.com
#   Date    :   17/10/27 10:47:13
#   Desc    :   Import urls to mongodb
#

import os
from itertools import islice
from sys import argv
import pymongo
import chardet

client = pymongo.MongoClient('localhost', 27017)
collection = client["tzd"]["obj"]

# Modified by neverstop at 2018.09.26 to support full paths
# UrlPath = './' + argv[1]
UrlPath = argv[1]
FileNames = os.listdir(UrlPath)

dup_num = 0
for file_name in FileNames:
    fullfilename = os.path.join(UrlPath,file_name)
    urlfile = open(fullfilename, 'r')
    for line in islice(urlfile,1,None):
        urlinfo = line.strip().split(',')
        coding = chardet.detect(urlinfo[1])['encoding']
        #add by Zhipeng 2018-03-22
        #urldict = {'batch_id':eval(urlinfo[0]), 'side': 'inside', 'webpage_url':urlinfo[1].decode(coding).encode('utf-8')}
        urldict = {'batch_id':urlinfo[0].strip('"'), 'webpage_url':urlinfo[1].strip('"').decode(coding).encode('utf-8')}
        '''if coding == 'ISO-8859-1':
            urldict = {'batch_id':urlinfo[0],'webpage_url':urlinfo[1].decode('ISO-8859-1').encode('utf-8')}
        elif coding == 'ISO-8859-9':
            urldict = {'batch_id':urlinfo[0],'webpage_url':urlinfo[1].decode('ISO-8859-9').encode('utf-8')}
        elif coding == 'GB2312':
            urldict = {'batch_id':urlinfo[0],'webpage_url':urlinfo[1].decode('GB2312').encode('utf-8')}
        elif coding == 'KOI8-R':
            urldict = {'batch_id':urlinfo[0],'webpage_url':urlinfo[1].decode('KOI8-R').encode('utf-8')}
        else:
            urldict = {'batch_id':urlinfo[0],'webpage_url':urlinfo[1].decode('ascii').encode('utf-8')}
        '''
        if collection.find(urldict).count() >= 1:
            print '此条数据已存在:', urldict
            dup_num += 1
            continue
        elif collection.find(urldict).count() == 0:
            print urldict
            collection.insert(urldict)
print
print '已存在的数据数量为:', dup_num


